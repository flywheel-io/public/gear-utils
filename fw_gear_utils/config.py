"""Config module."""
import json
from pathlib import Path

from .manifest import Manifest, ManifestValidationError


class Config:
    """Basic Config class akin to Manifest class.

    This class is mainly used as a helper for the
    `gear config create` function of the python-cli,
    however it may have unforseen programmatic usages.

    There is some confusing nomenclature here.  The gear
    configuration is stored in a file named `config.json`,
    within the config.json file, there are at three keys:
        * config:  configuration options for the gear,
            often booleans, integers, floats, or short
            strings.
        * inputs: Gear inputs, usually in the form of files,
            longer text.
        * destination: Information on where the gear is
            being run within the flywheel hierarchy.

    This class represents the `config.json` file, meaning
    that is has attributes which store the configuration
    options, inputs, and destination. That means that the
    class `Config` has a `config` attribute (configuration
    options) and the constructor accepts a `config`
    dictionary that contains a `config` (configuration
    options) key.
    """

    def __init__(  # pylint: disable=too-many-branches
        self, config=None, path=Path.cwd()
    ):
        """Generates initial config for gear.

        Args:
            config (dict, optional): Config dictionary with any of
                the following keys: 'config','inputs','destination'.
                Defaults to None.
            path (str or pathlib.Path, optional): Path to existing
                configuration or path to directory in which to create
                config. Defaults to Path.cwd().

        Raises:
            ConfigValidationError:
                1. When a the config kwarg has been passed but is
                    not of type dict.
                2. When a config.json exists at the passed path
                    but it is not valid JSON
                3. When a file is passed in for path, but it does
                    not exist.
        """
        self._config = dict()
        self._inputs = dict()
        self._destination = dict()
        self._path = Path(path)

        if config is not None:
            if isinstance(config, dict):
                if "config" in config:
                    self.config = config.get("config")
                if "inputs" in config:
                    self.inputs = config.get("inputs")
                if "destination" in config:
                    self.destination = config.get("destination")
            else:
                raise ConfigValidationError("Passed in", ["Cannot read config"])
        else:
            if self._path.is_dir():
                self._path = self._path / "config.json"
            if self._path.exists() and self._path.is_file():

                with open(self._path, "r") as fp:
                    try:
                        config = json.load(fp)
                        if "config" in config:
                            self.config = config.get("config")
                        if "inputs" in config:
                            self.inputs = config.get("inputs")
                        if "destination" in config:
                            self.destination = config.get("destination")
                    except json.JSONDecodeError as e:
                        raise ConfigValidationError(
                            self._path, ["Cannot read config file"]
                        ) from e
            else:
                raise ConfigValidationError(self._path, ["File doesn't exist"])

    @property
    def config(self):
        """Return full config.json."""
        return self._config

    @property
    def inputs(self):
        """Return inputs."""
        return self._inputs

    @property
    def destination(self):
        """Return destination."""
        return self._destination

    @config.setter  # type: ignore
    def config(self, new_config):
        """Set full config.json."""
        self._config = new_config

    @inputs.setter  # type: ignore
    def inputs(self, new_inputs):
        """Set full inputs."""
        self._inputs = new_inputs

    @destination.setter  # type: ignore
    def destination(self, dest):
        """Set destination."""
        self._destination = dest

    def update_config(self, vals):
        """Update full config.json."""
        self._config.update(vals)

    def update_destination(self, dest):
        """Update destination."""
        self._destination.update(dest)

    def add_input(self, name, val, type_="file", api=False):
        """Add an input to the config.

        Args:
            name (str): name of input
            val (str): input value, file path, api-key, context
            type_ (str, optional): file, api-key, or context.
                Defaults to "file".
            api (bool, optional): whether or not to connect to
                core to help generate config. Defaults to False.
                NOT YET IMPLEMENTED.

        Raises:
            ValueError: When file can't be resolved
            NotImplementedError: If api == True, not implemented yet.
        """
        if type_ == "file":
            if api:
                raise NotImplementedError(
                    "Adding api and context inputs is not yet supported"
                )
            path = Path(val)
            try:
                stat_result = path.resolve().stat()
            except FileNotFoundError as e:
                raise ValueError(
                    f"Cannot resolve file input at {path}, is the path correct?"
                ) from e

            file = {
                "base": "file",
                "location": {
                    "name": name,
                    "path": f"/flywheel/v0/input/{name}/{path.name}",
                },
                "object": {"size": stat_result.st_size, "type": None},
            }
            self.inputs.update({name: file})
        elif type_ == "api-key":
            self.inputs.update({name: {"base": "api-key", "key": val}})
        else:
            raise NotImplementedError("Adding context inputs is not yet supported")

    @classmethod
    def default_config_from_manifest(cls, manifest):
        """Create a default config.json from a manifest file.

        Args:
            manifest (str, or pathlib.Path or Manifest): Path to
                manifest or instantiated Manifest object.

        Raises:
            ValueError: When there is a problem parsing the
                manifest.

        Returns:
            Config: new config class with a default
                configuration.
        """
        if not isinstance(manifest, Manifest):
            try:
                manifest = Manifest(manifest)
            except ManifestValidationError as e:
                raise ValueError("Could not load manifest to generate config") from e

        config = {}

        for k, v in manifest.config.items():
            if "default" in v:
                config[k] = v["default"]

        return cls(config={"config": config})

    ############### Utilities
    def to_json(self, path=None):
        """Write config to json at path."""
        to_write = {
            "config": self.config,
            "inputs": self.inputs,
            "destination": self.destination,
        }
        with open(path if path is not None else str(self._path), "w") as fp:
            json.dump(to_write, fp, indent=4)

    def __str__(self):  # pragma: no cover
        """String representation."""
        to_write = {
            "config": self.config,
            "inputs": self.inputs,
            "destination": self.destination,
        }
        return json.dumps(to_write, indent=4)


class ConfigValidationError(Exception):
    """Indicates that the file at path is invalid.

    Attributes:
        path (str): The path to the file
        errors (list(str)): The list of error messages
    """

    def __init__(self, path, errors):
        """Initialize ConfigValidationError."""
        super().__init__()
        self.path = path
        self.errors = errors

    def __str__(self):
        """String representation."""
        result = "The config at {} is invalid:".format(self.path)
        for error in self.errors:
            result += "\n  {}".format(error)
        return result
