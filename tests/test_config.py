import json

# pylint: disable=protected-access
from pathlib import Path

import pytest

from fw_gear_utils.config import Config, ConfigValidationError
from fw_gear_utils.manifest import Manifest, ManifestValidationError


def test_init_config_not_dict():
    config = list()
    with pytest.raises(ConfigValidationError):
        _ = Config(config=config)


def test_init_config_not_exist(tmp_path):
    path = tmp_path
    with pytest.raises(ConfigValidationError):
        _ = Config(path=path)


@pytest.mark.parametrize(
    "config,inputs,destination",
    [
        ({}, [], {}),
        ({"test": "test"}, [], {}),
        ({}, [{"test": "test"}], {}),
        ({}, {}, {"test": "test"}),
        ({"test": "test"}, [{"test": "test"}], {"test": "test"}),
    ],
)
def test_init_config(config, inputs, destination):
    conf = Config(
        config={
            "config": config,
            "inputs": inputs,
            "destination": destination,
        }
    )

    assert conf._config == config
    assert conf._inputs == inputs
    assert conf._destination == destination


@pytest.mark.parametrize("f_type", ["dir", "file"])
def test_init_path_files(mocker, tmp_path, f_type):
    json_mock = mocker.patch("json.load")

    if f_type == "dir":
        conf_path = Path(tmp_path).resolve()
        (conf_path / "config.json").touch()
    else:
        conf_path = Path(tmp_path / "config.json").resolve()
        conf_path.touch()

    conf = Config(path=conf_path)
    if f_type == "dir":
        assert conf._path == conf_path / "config.json"
    else:
        assert conf._path == conf_path
    json_mock.assert_called_once()


@pytest.mark.parametrize(
    "config,inputs,destination",
    [
        ({}, {}, {}),
        ({"test": "test"}, {}, {}),
        ({}, [{"test": "test"}], {}),
        ({}, {}, {"test": "test"}),
        ({"test": "test"}, [{"test": "test"}], {"test": "test"}),
    ],
)
def test_init_path_json(tmp_path, config, inputs, destination):
    config_path = Path(tmp_path / "config.json")

    with open(str(config_path), "w") as fp:
        json.dump(
            {
                "config": config,
                "inputs": inputs,
                "destination": destination,
            },
            fp,
        )

    conf = Config(path=config_path)

    assert conf.config == config
    assert conf.inputs == inputs
    assert conf.destination == destination


def test_init_path_invalid_json(tmp_path):
    config_path = Path(tmp_path / "config.json")
    with open(str(config_path), "w") as fp:
        fp.write("[]}")

    with pytest.raises(ConfigValidationError):
        _ = Config(path=config_path)


@pytest.mark.parametrize("fn", ["set", "update"])  # Test both set and update
@pytest.mark.parametrize(
    "existing, update",
    [
        [{"test1": 3}, {"test2": 4}],  # Add new key,value
        [{"test1": 3}, {"test1": 4}],  # Update existing key/value
    ],
)
def test_set_config(existing, update, fn):
    conf = Config(config={"config": existing})

    if fn == "update":
        conf.update_config(update)
        existing.update(update)
        assert conf.config == existing
    else:
        conf.config = update
        assert conf.config == update


@pytest.mark.parametrize(
    "existing, update",
    [
        [{"test1": 3}, {"test2": 4}],  # Add new key,value
        [{"test1": 3}, {"test1": 4}],  # Update existing key/value
    ],
)
def test_set_inputs(existing, update):
    conf = Config(config={"inputs": [existing]})
    conf.inputs = [update]

    assert conf.inputs == [update]


@pytest.mark.parametrize("fn", ["set", "update"])  # Test both set and update
@pytest.mark.parametrize(
    "existing, update",
    [
        [{"test1": 3}, {"test2": 4}],  # Add new key,value
        [{"test1": 3}, {"test1": 4}],  # Update existing key/value
    ],
)
def test_set_destination(existing, update, fn):
    conf = Config(config={"destination": existing})

    if fn == "update":
        conf.update_destination(update)
        existing.update(update)
        assert conf.destination == existing
    else:
        conf.destination = update
        assert conf.destination == update


@pytest.fixture
def empty_config(tmp_path):
    with open(f"{tmp_path}/config.json", "w") as fp:
        json.dump({"config": {}, "inputs": {}, "destination": {}}, fp)

        return f"{tmp_path}/config.json"


@pytest.mark.parametrize("file_t, api", [["api", False], ["file", True]])
def test_not_implemented(empty_config, file_t, api):
    conf = Config(path=empty_config)
    with pytest.raises(NotImplementedError):
        conf.add_input("test", "test", type_=file_t, api=api)


def test_file_not_resolve(empty_config, tmp_path):
    val = Path(tmp_path) / "test"
    conf = Config(path=empty_config)
    with pytest.raises(ValueError):
        conf.add_input("test", str(val), type_="file")


def test_add_input(empty_config, tmp_path):
    path = Path(tmp_path / "tmp.txt")
    with open(str(path), "w") as fp:
        fp.write("hello")
    size = path.stat().st_size
    conf = Config(path=empty_config)
    conf.add_input("test", str(path), type_="file")

    assert len(conf.inputs) == 1
    input1 = conf.inputs["test"]
    assert input1["base"] == "file"
    assert input1["location"]["name"] == "test"
    assert input1["location"]["path"] == "/flywheel/v0/input/test/tmp.txt"
    assert input1["object"]["size"] == size


@pytest.fixture
def manifest():
    return (
        {
            "fractional_intensity_threshold": {
                "default": 0.5,
                "minimum": 0,
                "maximum": 1,
                "type": "number",
            },
            "brain_surf_outline": {"default": False, "type": "boolean"},
            "binary_brain_mask": {"default": False, "type": "boolean"},
            "skull_image": {"default": False, "type": "boolean"},
            "vertical_gradient_intensity_threshold": {
                "default": 0,
                "minimum": -1,
                "maximum": 1,
                "type": "number",
            },
            "apply_mask_thresholding": {"default": False, "type": "boolean"},
            "vtk_surface_mesh": {"default": False, "type": "boolean"},
            "center": {"optional": True, "type": "string"},
            "radius": {"optional": True, "type": "integer"},
            "function_option": {"default": "", "type": "string"},
        },
        {
            "fractional_intensity_threshold": 0.5,
            "brain_surf_outline": False,
            "binary_brain_mask": False,
            "skull_image": False,
            "vertical_gradient_intensity_threshold": 0,
            "apply_mask_thresholding": False,
            "vtk_surface_mesh": False,
            "function_option": "",
        },
    )


@pytest.mark.parametrize(
    "side_effect, expected",
    [
        (FileNotFoundError(), FileNotFoundError),
        (ManifestValidationError("asdf", "asdf"), ValueError),
    ],
)
def test_default_error(mocker, side_effect, expected):
    manifest_mock = mocker.patch("fw_gear_utils.config.Manifest")
    # Hacky, but works
    manifest_mock.__bases__ = Manifest.__bases__
    manifest_mock.side_effect = side_effect
    with pytest.raises(expected):
        Config.default_config_from_manifest("test")


def test_class_returned(mocker, manifest):
    manifest_mock = mocker.patch("fw_gear_utils.config.Manifest")
    # Hacky, but works
    manifest_mock.__bases__ = Manifest.__bases__
    manifest_mock.return_value.config = manifest[0]

    conf = Config.default_config_from_manifest("nothing")
    assert conf.config == manifest[1]


@pytest.mark.parametrize("file_path", [None, "test"])
def test_to_json(mocker, tmp_path, file_path):
    json_mock = mocker.patch("json.dump")
    open_mock = mocker.patch("builtins.open")
    my_conf = {
        "config": {"test": "test"},
        "inputs": [{"test", "test"}],
        "destination": {"test": "test"},
    }
    conf = Config(config=my_conf)
    if file_path:
        my_file = Path(tmp_path / file_path).resolve()
        conf.to_json(path=str(my_file))
        open_mock.assert_called_once_with(str(my_file), "w")
    else:
        conf.to_json()
        open_mock.assert_called_once_with(str(Path.cwd()), "w")
    # assert json_mock.call_args.args[0] == my_conf # Doesn't work in py3.7
    assert json_mock.call_args[0][0] == my_conf


def test_config_validation_error():
    str_repr = ""
    try:
        raise ConfigValidationError("test_path", ["test"])
    except ConfigValidationError as e:
        str_repr = str(e)

    assert str_repr == "The config at test_path is invalid:\n  test"
