from pathlib import Path

import pytest

TEST_ROOT = Path(__file__).parent
DATA_ROOT = TEST_ROOT / "data"


@pytest.fixture(scope="function")
def json_file():
    def get_manifest_file(name):
        return str(DATA_ROOT / f"{name}.json")

    return get_manifest_file
