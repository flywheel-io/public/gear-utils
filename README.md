# Flywheel Gear Utils

This repository contains Flywheel gear utils with minimal dependencies,
i.e. no dependency on Flywheel site version, or SDK version.

Currently the repo contains:

* [`Manifest`](#Manifest): A class to interact with a gear Manifest
* [`Config`](#Config): A class to interact with a gear Config

## Manifest

The `Manifest` class provides utilities for
parsing and generating a manifest for a gear. Including functionality to:

* Load a manifest from a file
* Check if the manifest is valid
* Create default config schema from manifest
* Access properties for docker image, version, inputs, license, etc.

Ex.:

```python

    >>> from flywheel_gear_toolkit.utils.manifest import Manifest
    >>> manifest = Manifest(<manifest_file>)
    >>> manifest.is_valid()
    []  # No errors
    True
```

## Config

The `Config` class provides utilities for
parsing and generating a manifest for a gear. Including functionality to:

* Generate default config from manifest
* Access config properties
* Utilities for building a `config.json` file.

Ex.:

```python

    >>> from flywheel_gear_toolkit.utils import config, manifest
    >>> m = manifest.Manifest(<manifest_file>)
    >>> c = config.Config.default_config_from_manifest(m)
    >>> c.add_input('dicom','~/input_dicom.dcm.zip')
    >>> c.to_json('~/gear/config.json')

```
